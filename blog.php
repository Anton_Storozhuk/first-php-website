<?php

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_stratup_errors', 1);

    require_once 'app/header.php';

    $get_posts = get_posts(4);

    $posts = $get_posts[0];
    $page = $get_posts[1];
    $total_pages = $get_posts[2];
?>

<main>    
    <div class="wrapper">
        <h1>Blog</h1>
        <div class="postsList">
            <?php foreach ($posts as $post): ?>
                <div class="postsList__item">
                    <a href="/post.php?post_id=<?= $post['id'] ?>" class="postsList__item_image">
                        <img src="<?= $post['image'] ?>" alt="post-image">
                    </a>
                    <div class="postsList__item_container">
                        <a href="/post.php?post_id=<?= $post['id'] ?>" class="postsList__item_title">
                            <h2><?= $post['title'] ?></h2>
                        </a>
                        <?php if($post['content'] && strlen($post['content']) > 256): ?>
                            <p><?= mb_substr($post['content'], 0, 256, 'UTF-8').'...' ?></p>
                        <?php else: ?>
                            <p><?= $post['content'] ?></p>
                        <?php endif; ?>
                        <a href="/post.php?post_id=<?= $post['id'] ?>" class="button">Read more</a>
                    </div>
                </div>
            <?php endforeach; ?>
            <ul class="pagination">
                <?php for ($i = 1; $i <= $total_pages; $i++): ?>
                    <li class="<?= $page == $i ? 'active' : '' ?>">
                        <a href="<?='?page='.($i) ?>"><?= $i ?></a>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
</main>

<?php include_once 'app/footer.php' ?>