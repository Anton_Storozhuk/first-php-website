<?php
    function get_posts($posts_quantity_per_page) {
        global $link;

        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $offset = ($page - 1) * $posts_quantity_per_page;
        $posts = mysqli_fetch_all(mysqli_query($link, "SELECT * FROM posts ORDER BY id DESC LIMIT $offset, $posts_quantity_per_page"), 1);

        $total_pages = ceil(mysqli_fetch_array(mysqli_query($link, "SELECT COUNT(*) FROM posts"))[0] / $posts_quantity_per_page);

        return [$posts, $page, $total_pages];
    }


    function get_posts_by_id($post_id) {
        global $link;

        $post = mysqli_fetch_assoc(mysqli_query($link, "SELECT * FROM posts WHERE id = $post_id"));

        return $post;
    }

    function get_recent_posts($posts_quantity) {
        global $link;
    
        $posts = mysqli_fetch_all(mysqli_query($link, "SELECT * FROM posts ORDER BY id DESC LIMIT $posts_quantity"), 1);
    
        return $posts;
    }

    function insert_info($email, $tel, $text) {
        global $link;
    
        $email = mysqli_real_escape_string($link, $email);
        $tel = mysqli_real_escape_string($link, $tel);
        $text = mysqli_real_escape_string($link, $text);
            
        $result = mysqli_query($link, "INSERT INTO info (email, tel, text) VALUES ('$email', '$tel', '$text')");

        if ($result) { 
            return 'success';
        } else {
            return 'unsuccess';
        }
    }

    function redirect_to_404() {
        http_response_code(404);
        include './404.php';
        exit;
    }
?>