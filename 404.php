<?php

require_once 'app/header.php';

?>

<main class="error-page">
    <div class="wrapper">
        <img src="<?php echo '/assets/images/404.png' ?>" alt="404">
    </div>
</main>

<?php include_once 'app/footer.php' ?>