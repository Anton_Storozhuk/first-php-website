<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_stratup_errors', 1);

require_once 'app/header.php';

    if (isset($_POST['email']) && isset($_POST['tel']) && isset($_POST['text'])) {
        $email = trim($_POST['email']);
        $tel = trim($_POST['tel']);
        $text = trim($_POST['text']);

        $result = insert_info($email, $tel, $text);
        header('/contact.php');
    }

?>

<main>
    <div class="wrapper">
        <h1>Contact us</h1>
        <form action="/contact.php" method="POST">
            <input type="email" name="email" value="" placeholder="Enter your email" required>
            <input type="tel" name="tel" value="" placeholder="Enter your phone number" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
            <textarea name="text" placeholder="Enter your message" cols="30" rows="10" required></textarea>
            <button class="button" type="submit">Submit</button>
        </form>

        <p class="result-message<?= $result == 'success' ? ' success' : ' fail' ?>">
            <?= $result ?? '' ?>
        </p>
    </div>
</main>

<?php include_once 'app/footer.php' ?>